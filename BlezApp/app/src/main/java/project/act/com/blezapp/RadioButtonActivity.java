package project.act.com.blezapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class RadioButtonActivity extends AppCompatActivity {

    private RadioGroup rg_sex;
    private RadioGroup rg_nationality;
    private String sex, nationality;
    private RadioButton radioButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio_button);

        rg_sex = (RadioGroup) findViewById(R.id.rg_sex);
        rg_nationality = (RadioGroup) findViewById(R.id.rg_nationality);

        Button btn_check = (Button) findViewById(R.id.btn_check);

        btn_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rg_sex.getCheckedRadioButtonId() != 0 && rg_nationality.getCheckedRadioButtonId() != 0) {
                    radioButton = (RadioButton) findViewById(rg_sex.getCheckedRadioButtonId());
                    sex = radioButton.getText().toString();
                    radioButton = (RadioButton) findViewById(rg_nationality.getCheckedRadioButtonId());
                    nationality = radioButton.getText().toString();

                    Toast.makeText(RadioButtonActivity.this, "sex = " + sex + ", nationality = " + nationality, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(RadioButtonActivity.this, "Please checked the fill first!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
